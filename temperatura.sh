echo -n "Ingrese temperatura en Celcius "
read tm
echo -n "Ingrese F si desea pasarlo a Fahrenheit o K si desea pasarlo a Kelvin "
read vl

if [ $vl -eq "F"]; then 
	 fah=$(($(9/5)*$tm)+32))
	echo "La temperatura en Fahrenheit es $fah"
elif [ $vl -eq "K"]; then
	 tc=$(($(5/9)*$($tm-32)))
	 echo "La temperatura en Kelvin es $tc"
else
	echo "Ingrese K o F"
fi
